#include <stdio.h>
#include <stdlib.h>
#include <string.h>
static int o = 14;//已经录入车辆，初始化文档读入
static int p = 6;//已经录入乘客

void ex();//退出
void funtion0();//座位数查找班车
void funtion7();//车辆ID查找
void funtion8();//车辆状态查找
void showmeun();//主菜单
void intputbus();//车辆录入
void intputpeople();//乘客信息录入
void search();//车辆查找     主菜单
void seek();//乘客查找
void meun1();//车辆子菜单
void meun2();//乘客子菜单
void seek1();//按姓名查找
void seek2();//按乘坐时间查找
void seek3();//按乘客乘坐ID查找
void funtion3();//车辆信息修改
void funtion4();//乘客信息修改
void funtion5();//信息排序
void funtion6();//自动排班
void delcar();//删除车辆
void delpeo();//删除乘客
void delpeosj();//按乘坐时间删除乘客
void delpeoyx();//按院系删除乘客
void delpeoid();//按乘客ID删除乘客
void delcarxh();//按车辆型号删除
void delcarzt();//按车辆状态删除
void delcarid();//按车辆id删除
void look1();//浏览所有车辆信息
void look2();//浏览所有乘客信息
void outfile();//文件导出主菜单
void putcar();//导出车辆文件
void putpeo();//导出乘客文件
void incar();//读入本地车辆信息文件
void inpeople();//读入本地乘客信息
struct BUS {             //结构体车
	char id[10];
	char xh[10];
	char ys[10];
	char zws[5];
	char cph[10];
	char zt[5];
};

struct  PEOPLE {           //结构体人
	char id[10];
	char xm[10];
	char gh[20];
	char yx[10];
	char czsj[10];
};
struct BUS car[25];
struct PEOPLE peo[100];


int main() {
	incar() ;
	inpeople();           										                	//主函数
	showmeun();
	return 0;
}

void incar() {
	int i;
	FILE *fp = NULL;
	if ((fp = fopen("incars.txt", "r")) ==
	        NULL) {                              //incars.txt文件需要存在（incars.txt为本地车辆信息文件）
		printf("打开文件失败！");
	}
	fscanf(fp, "%s", car[0].id);
	fscanf(fp, "%s", car[0].xh);
	fscanf(fp, "%s", car[0].ys);
	fscanf(fp, "%s", car[0].zws);               //先读取头文件，头文件易于文本表格阅读
	fscanf(fp, "%s", car[0].cph);
	fscanf(fp, "%s", car[0].zt);
	for (i = 0; i < 15; i++) {
		fscanf(fp, "%s", car[i].id);
		fscanf(fp, "%s", car[i].xh);
		fscanf(fp, "%s", car[i].ys);
		fscanf(fp, "%s", car[i].zws);                //覆盖头文件
		fscanf(fp, "%s", car[i].cph);
		fscanf(fp, "%s", car[i].zt);
	}
}

void showmeun() {                                                                             //主菜单
	char n;
	system("cls");
	printf("\n\t\t**********欢迎使用班车管理系统**********\n");
	printf("\t\t┌---------------------------------┐\n");
	printf("\t\t│        1.车辆信息               │\n");
	printf("\t\t│        2.乘客信息               │\n");
	printf("\t\t│        3.信息导出               │\n");
	printf("\t\t│        4.退出                   │\n");
	printf("\t\t└---------------------------------┘\n");
	printf("\n\t\t**********班车管理系统*******************\n");
	printf("\t\t请您选择(1-4):");
	scanf("%c", &n);
	while (n != 9) {
		switch (n) {
			case '1':
				meun1();
				break;
			case '2':
				meun2();
				break;
			case '3':
				outfile();
				break;
			case '4':
				system("cls");
				ex();
				break;
			default:
				break;
		}
		scanf("%c", &n);
	}
}

void ex() {
	int i;
	printf("输入“0”退出系统，输入“1”则返回主菜单。\n");
	printf("请输入：");
	scanf("%d", &i);                                                             //退出函数
	if (i == 0) {
		printf("************欢迎再次使用！**************\n");
		printf("            ˉ\\_(ツ)_/ˉ\n");
		system("pause");
		exit(1);
	} else {
		showmeun();
	}
}

void meun1() {
	system("cls");
	char n;
	printf("\t\t┌-------------------------------┐\n");
	printf("\t\t│        1.车辆录入             │\n");
	printf("\t\t│        2.车辆查找             │\n");
	printf("\t\t│        3.车辆信息修改         │\n");                                    //车辆菜单
	printf("\t\t│        4.浏览所有车辆         │\n");
	printf("\t\t│        5.删除车辆             │\n");
	printf("\t\t│        6.返回                 │\n");
	printf("\t\t└-------------------------------┘\n");
	printf("\t\t请您选择(1-6):");
	scanf("%c", &n);
	while (1) {
		switch (n) {
			case '1':
				intputbus();
				break;
			case '2':
				search();
				break;
			case '3':
				funtion3();
				break;
			case '4':
				look1();
				break;
			case '5':
				delcar();
				break;
			case '6':
				system("cls");
				showmeun();
				break;
			default:
				break;
		}
		scanf("%c", &n);
	}
}

void meun2() {                                                                //乘客菜单
	system("cls");
	char n;
	printf("\t\t┌-------------------------------┐\n");
	printf("\t\t│        1.乘客录入             │\n");
	printf("\t\t│        2.乘客查找             │\n");
	printf("\t\t│        3.乘客信息修改         │\n");
	printf("\t\t│        4.浏览所有乘客预约     │\n");
	printf("\t\t│        5.删除乘客信息         │\n");
	printf("\t\t│        6.返回                 │\n");
	printf("\t\t└-------------------------------┘\n");
	printf("\t\t请您选择(1-6):");
	scanf("%c", &n);
	while (n) {
		switch (n) {
			case '1':
				intputpeople();
				break;
			case '2':
				seek();
				break;
			case '3':
				funtion4();
				break;
			case '4':
				look2();
				break;
			case '5':
				delpeo();
				break;
			case '6':
				system("cls");
				showmeun();
				break;
			default:
				break;
		}
		scanf("%c", &n);
	}
}

void intputbus() {
	int i, a, k, m = 1;
	printf("请输入本次要录入车辆的数量(请输入正整数)：");
	scanf("%d", &a);
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	o += a;
	for (i = o - a; i < o; i++) {
		printf("第%d辆车信息录入\n", m++);
		printf("请输入车辆ID    ：");                                                //车辆录入
		scanf("%s", car[i + 1].id);
		printf("请输入车辆型号  ：");
		scanf("%s", car[i + 1].xh);
		printf("请输入车辆颜色  ：");
		scanf("%s", car[i + 1].ys);
		printf("请输入车辆座位数：");
		scanf("%s", car[i + 1].zws);
		printf("请输入车辆车牌号：");
		scanf("%s", car[i + 1].cph);
		printf("请输入车辆的状态：");
		scanf("%s", car[i + 1].zt);
		printf("\n");
	}
	printf("%d辆信息成功录入。", a);
	printf("\n\n本次录已入%d车辆信息：\n", a);
	for (i = o - a; i < o; i++) {
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		printf("车辆ID：%s\n", car[i + 1].id);
		printf("型号  ：%s\n", car[i + 1].xh);
		printf("颜色  ：%s\n", car[i + 1].ys);
		printf("座位数：%s\n", car[i + 1].zws);
		printf("车牌号：%s\n", car[i + 1].cph);
		printf("状态  ：%s\n", car[i + 1].zt);
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
	}
	printf("继续录入请输入‘1’，返回上一页菜单请输入‘2’：");
	scanf("%d", &k);
	if (k == 1) {
		intputbus();
	} else {
		meun1();
	}
}

void outfile() {
	system("cls");
	char n;
	printf("\t\t┌-------------------------------┐\n");
	printf("\t\t│        1.车辆信息导出         │\n");
	printf("\t\t│        2.乘客信息导出         │\n");                                //文件导出主菜单
	printf("\t\t│        3.返回                 │\n");
	printf("\t\t└-------------------------------┘\n");
	printf("\t\t请您选择(1-3):");
	scanf("%c", &n);
	while (n) {
		switch (n) {
			case '1':
				putcar();
				break;
			case '2':
				putpeo();
				break;
			case '3':
				system("cls");
				showmeun();
				break;
			default:
				break;
		}
		scanf("%c", &n);
	}
}

void putcar() {
	FILE *fp;
	int i;
	if ((fp = fopen("car.txt", "w")) == NULL) {
		printf("导出文件信息失败!\n");                           //车辆文件导出
		exit(0);
	}
	fprintf(fp, "车辆ID		型号				颜色		座位数		车牌号					状态\n");

	for (i = 0; i < 25; i++) {
		fprintf(fp, " %s			%s				%s		%s			%s					%s\n", car[i].id, car[i].xh, car[i].ys, car[i].zws, car[i].cph, car[i].zt);
	}
	fclose(fp);
	printf("文件导出完成,文件名为car.txt\n保存路径：D:\\Temp\\car.txt\n");
	system("pause");
	showmeun();
}

void intputpeople() {                                              //乘客录入
	int i, a, k, m = 1;
	printf("请输入本次要录入乘客的数量(请输入正整数)：");
	scanf("%d", &a);
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	p += a;
	for (i = p - a; i < p; i++) {
		printf("第%d个乘客信息录入\n", m++);
		printf("请输入乘客ID      ：");                                                //车辆录入
		scanf("%s", peo[i].id);
		printf("请输入乘客姓名    ：");
		scanf("%s", peo[i].xm);
		printf("请输入乘客工号    ：");
		scanf("%s", peo[i].gh);
		printf("请输入乘客院系    ：");
		scanf("%s", peo[i].yx);
		printf("请输入乘客乘坐时间：");
		scanf("%s", peo[i].czsj);
		printf("\n");
	}
	printf("%d个乘客信息成功录入。", a);
	printf("\n\n本次已录入%d个乘客：\n", a);
	for (i = p - a; i < p; i++) {
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		printf("ID    ：%s\n", peo[i].id);
		printf("姓名  ：%s\n", peo[i].xm);
		printf("工号  ：%s\n", peo[i].gh);
		printf("院系  ：%s\n", peo[i].yx);
		printf("时间  ：%s\n", peo[i].czsj);
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
	}
	printf("继续录入请输入‘1’，返回上一页菜单请输入‘2’：");
	scanf("%d", &k);
	if (k == 1) {
		intputpeople();
	} else {
		meun2();
	}
}

void search() {                                                         //车辆查找
	{
		system("cls");
		char n;
		printf("\t\t┌-------------------------------┐\n");
		printf("\t\t│        1.车辆座位数查找       │\n");
		printf("\t\t│        2.空闲车辆查找         │\n");             //文件导出主菜单
		printf("\t\t│        3.车辆ID数查找         │\n");
		printf("\t\t│        4.返回主菜单           │\n");
		printf("\t\t└-------------------------------┘\n");
		printf("\t\t请您选择(1-4):");
		scanf("%c", &n);
		while (n) {
			switch (n) {
				case '1':
					funtion0();
					break;
				case '2':
					funtion7();
					break;
				case '3':
					funtion8();
					break;
				case '4':
					system("cls");
					showmeun();
					break;
				default:
					break;
			}
			scanf("%c", &n);
		}
	}
}

void funtion0() {                                        //座位数查找车辆
	int i, j = 0;
	char k[4];
	printf("请输入查找车辆座位数(如用户没有添加车辆，默认座位数有：10~15~20。)：");
	scanf("%s", &k);
	printf("\n");
	for (i = 0; i <= o; i++) {
		if (0 == strcmp(car[i].zws, k)) {
			j++;
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("车辆ID：%s\n", car[i].id);
			printf("型号  ：%s\n", car[i].xh);
			printf("颜色  ：%s\n", car[i].ys);
			printf("牌号  ：%s\n", car[i].zws);
			printf("牌号  ：%s\n", car[i].cph);
			printf("状态  ：%s\n", car[i].zt);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		}
	}
	if (j == 0) {
		printf("此座位数的车辆不存在！！！\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();
	}
	printf("任意键返回主菜单。\n");
	system ("pause");
	showmeun();
}

void seek() {                                       //乘客查找
	{
		system("cls");
		char n;
		printf("\t\t┌-------------------------------┐\n");
		printf("\t\t│        1.乘客姓名查找         │\n");
		printf("\t\t│        2.乘坐时间查找         │\n");             //文件导出主菜单
		printf("\t\t│        3.乘客ID数查找         │\n");
		printf("\t\t│        4.返回主菜单           │\n");
		printf("\t\t└-------------------------------┘\n");
		printf("\t\t请您选择(1-4):");
		scanf("%c", &n);
		while (n) {
			switch (n) {
				case '1':
					seek1();
					break;
				case '2':
					seek2();
					break;
				case '3':
					seek3();
					break;
				case '4':
					system("cls");
					showmeun();
					break;
				default:
					break;
			}
			scanf("%c", &n);
		}
	}
}


void funtion1() {
	printf("等待开发！！！");                                     //信息排序功能
}

void funtion3() {                                                 //车辆信息修改
	int i, j = 0, c;
	char k[4];
	printf("请输入需要修改信息的车辆ID(两位数如:01)：");
	scanf("%s", &k);
	printf("\n");
	for (i = 0; i < o + 1; i++) {
		if (0 == strcmp(k, car[i].id)) {
			j++;
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("车辆ID：%s\n", car[i].id);
			printf("型号  ：%s\n", car[i].xh);
			printf("颜色  ：%s\n", car[i].ys);
			printf("座位数：%s\n", car[i].zws);
			printf("牌号  ：%s\n", car[i].cph);
			printf("状态  ：%s\n", car[i].zt);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			break;
		}
	}
	if (j == 0) {
		printf("此ID车不存在！！！\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();
	} else {
		printf("已定位到当前车辆,请输入需要修改的信息\n");
		printf("修改ID    ，请输入1\n");
		printf("修改型号  ，请输入2\n");
		printf("修改颜色  ，请输入3\n");
		printf("修改座位数，请输入4\n");
		printf("修改车牌号，请输入5\n");
		printf("修改状态  ，请输入6\n");
		printf("返回主菜单，请输入7\n");
		printf("\n请您选择(1-7):");
	}
	scanf("%d", &c);
	switch (c) {
		case 1:
			printf("\n\t\t\t\t请输入修改后的车辆ID：");
			scanf("%s", &car[i].id);
			printf("修改成功");
			break;
		case 2:
			printf("\n\t\t\t\t请输入修改后的型号：");
			scanf("%s", &car[i].xh);
			printf("修改成功");
			break;
		case 3:
			printf("\n\t\t\t\t请输入修改后的颜色：");
			scanf("%s", &car[i].ys);
			printf("修改成功");
			break;
		case 4:
			printf("\n\t\t\t\t请输入修改后的座位数：");
			scanf("%s", &car[i].zws);
			printf("修改成功");
			break;
		case 5:
			printf("\n\t\t\t\t请输入修改后的车牌号：");
			scanf("%s", &car[i].cph);
			printf("修改成功");
			break;
		case 6:
			printf("\n\t\t\t\t请输入修改后的状态：");
			scanf("%s", &car[i].zt);
			printf("                      修改成功");
			break;
		case 7:
			showmeun();
			break;
		default:
			printf("\n\t\t\t\t无此选项！车辆信息未修改！！！");
	}                                                                       //switch函数结束括号
	printf("修改后的车辆信息为：\n");
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	printf("车辆ID：%s\n", car[i].id);
	printf("型号  ：%s\n", car[i].xh);
	printf("颜色  ：%s\n", car[i].ys);
	printf("座位数：%s\n", car[i].zws);
	printf("牌号  ：%s\n", car[i].cph);
	printf("状态  ：%s\n", car[i].zt);
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	printf("任意键返回主菜单。\n");
	system ("pause");
	showmeun();

}

void funtion4() {                                             //按ID信息修改乘客信息
	int i, j = 0, c;
	char k[4];
	printf("请输入需要修改信息的乘客ID(两位数如:01)：");
	scanf("%s", &k);
	printf("\n");
	for (i = 0; i < p; i++) {
		if (0 == strcmp(k, peo[i].id)) {
			j++;
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("ID    ：%s\n", peo[i].id);
			printf("姓名  ：%s\n", peo[i].xm);
			printf("工号  ：%s\n", peo[i].gh);
			printf("院系  ：%s\n", peo[i].yx);
			printf("时间  ：%s\n", peo[i].czsj);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
			break;
		}
	}
	if (j == 0) {
		printf("此ID乘客不存在！！！\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();
	} else {
		printf("已定位到当前乘客信息,请输入需要修改的信息\n");
		printf("修改ID      ，请输入1\n");
		printf("修改姓名    ，请输入2\n");
		printf("修改学号    ，请输入3\n");
		printf("修改院系    ，请输入4\n");
		printf("修改乘坐时间，请输入5\n");
		printf("返回主菜单  ，请输入6\n");
		printf("\n请您选择(1-6):");
	}
	scanf("%d", &c);
	switch (c) {
		case 1:
			printf("\n\t\t\t\t请输入修改后的乘客ID：");
			scanf("%s", &peo[i].id);
			printf("修改成功");
			break;
		case 2:
			printf("\n\t\t\t\t请输入修改后的姓名：");
			scanf("%s", &peo[i].xm);
			printf("修改成功");
			break;
		case 3:
			printf("\n\t\t\t\t请输入修改后的工号：");
			scanf("%s", &peo[i].gh);
			printf("修改成功");
			break;
		case 4:
			printf("\n\t\t\t\t请输入修改后的院系：");
			scanf("%s", &peo[i].yx);
			printf("修改成功");
			break;
		case 5:
			printf("\n\t\t\t\t请输入修改后的乘坐时间：");
			scanf("%s", &peo[i].czsj);
			printf("修改成功");
			break;
		case 6:
			showmeun();
			break;
		default:
			printf("\n\t\t\t\t无此选项！乘客信息未修改！！！");
	}                                                                       //switch函数结束括号
	printf("修改后的乘客信息为：\n");
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	printf("ID    ：%s\n", peo[i].id);
	printf("姓名  ：%s\n", peo[i].xm);
	printf("工号  ：%s\n", peo[i].gh);
	printf("院系  ：%s\n", peo[i].yx);
	printf("时间  ：%s\n", peo[i].czsj);
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
	printf("任意键返回主菜单。\n");
	system ("pause");
	showmeun();

}

void putpeo() {                                   //乘客信息导出文件
	FILE *fp;
	int i;
	if ((fp = fopen("people.txt", "w")) == NULL) {
		printf("导出文件信息失败!\n");                           //车辆文件导出
		exit(0);
	}
	fprintf(fp, "乘客ID		姓名				学号					院系				乘坐时间\n");

	for (i = 0; i < 25; i++) {
		fprintf(fp, " %s			%s			%s			%s			%s\n", peo[i].id, peo[i].xm, peo[i].gh, peo[i].yx, peo[i].czsj);
	}
	fclose(fp);
	printf("文件导出完成,文件名为people.txt\n保存路径：D:\\Temp\\people.txt\n");
	system("pause");
	showmeun();
}

void funtion8() {                                                  //ID查找车辆
	int i, j = 0;
	char k[4];
	printf("请输入查找车辆ID(两位数如:01)：");
	scanf("%s", &k);
	printf("\n");
	for (i = 0; i < o; i++) {
		if (0 == strcmp(k, car[i].id)) {
			j++;
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("车辆ID：%s\n", car[i].id);
			printf("型号  ：%s\n", car[i].xh);
			printf("颜色  ：%s\n", car[i].ys);
			printf("牌号  ：%s\n", car[i].zws);
			printf("牌号  ：%s\n", car[i].cph);
			printf("状态  ：%s\n", car[i].zt);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		}
	}

	if (j == 0) {
		printf("此ID车不存在！！！\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();
	}
	printf("任意键返回主菜单。\n");
	system ("pause");
	showmeun();
}

void funtion7() {                                                    //状态查找车辆
	int i;
	char k[6] = "空闲";
	for (i = 0; i <= o; i++) {
		if (0 == strcmp(k, car[i].zt)) {
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("车辆ID：%s\n", car[i].id);
			printf("型号  ：%s\n", car[i].xh);
			printf("颜色  ：%s\n", car[i].ys);
			printf("牌号  ：%s\n", car[i].zws);
			printf("牌号  ：%s\n", car[i].cph);
			printf("状态  ：%s\n", car[i].zt);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		}
	}
	printf("任意键返回主菜单。\n");
	system ("pause");
	showmeun();
}

void look1() {                               //浏览所有车辆信息
	int i;
	for (i = 0; i <= o; i++) {
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		printf("车辆ID：%s\n", car[i].id);
		printf("型号  ：%s\n", car[i].xh);
		printf("颜色  ：%s\n", car[i].ys);
		printf("座位数：%s\n", car[i].zws);
		printf("牌号  ：%s\n", car[i].cph);
		printf("状态  ：%s\n", car[i].zt);
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	}
	printf("任意键返回主菜单。\n");
	system ("pause");
	showmeun();
}

void delcar() {                              //删除车辆信息
	char n;
	system("cls");
	printf("\t\t┌---------------------------------┐\n");
	printf("\t\t│        1.按班车状态删除         │\n");
	printf("\t\t│        2.按班车型号删除         │\n");
	printf("\t\t│        3.按班车ID删除           │\n");
	printf("\t\t│        4.返回主菜单             │\n");
	printf("\t\t└---------------------------------┘\n");
	printf("\t\t请您选择(1-4):");
	while (1) {
		switch (n) {
			case '1':
				system("cls");
				delcarzt();
				break;
			case '2':
				system("cls");
				delcarxh();
				break;
			case '3':
				system("cls");
				delcarid();
				break;
			case '4':
				showmeun();
				break;
			default:
				break;
		}
		scanf("%c", &n);
	}

}

void delcarzt() {                                               //按状态删除车辆
	int a, i, k;
	char zt[10];
	printf("请输入要删除的状态班车(如：工作 维修)：");
	scanf("%s", &zt);
	for (i = 0; i <= o; i++) {
		if (0 == strcmp(zt, car[i].zt)) {
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("车辆ID：%s\n", car[i].id);
			printf("型号  ：%s\n", car[i].xh);
			printf("颜色  ：%s\n", car[i].ys);
			printf("牌号  ：%s\n", car[i].zws);
			printf("牌号  ：%s\n", car[i].cph);
			printf("状态  ：%s\n", car[i].zt);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		}
	}
	printf("已搜索完毕，显示如上，如果结果为空，请输入‘0’返回。\n");
	printf("输入‘1’确认删除该状态的车辆。\n");
	printf("输入‘0’，返回主菜单。\n");
	scanf("%d", &a);

	if (a == 1) {
		for (i = 0; i <= o; i++) {
			if (0 == strcmp(zt, car[i].zt)) {
				for (k = i; k <= o; k++) {
					strcpy (car[k].id, car[k + 1].id);
					strcpy (car[k].xh, car[k + 1].xh);
					strcpy (car[k].ys, car[k + 1].ys);
					strcpy (car[k].zws, car[k + 1].zws);
					strcpy (car[k].cph, car[k + 1].cph);
					strcpy (car[k].zt, car[k + 1].zt);
				}
			}
		}
		o -= 1;
		printf("已将该状态的车辆删除。\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();


	} else {
		showmeun();
	}
}

void delcarxh() {                                     //按型号删除车辆
	int a, i, k;
	char xh[10];
	printf("请输入要删除的型号：");
	scanf("%s", &xh);
	for (i = 0; i < o; i++) {
		if (0 == strcmp(xh, car[i].xh)) {
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("车辆ID：%s\n", car[i].id);
			printf("型号  ：%s\n", car[i].xh);
			printf("颜色  ：%s\n", car[i].ys);
			printf("牌号  ：%s\n", car[i].zws);
			printf("牌号  ：%s\n", car[i].cph);
			printf("状态  ：%s\n", car[i].zt);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		}
	}
	printf("已搜索完毕，显示如上，如果结果为空，请输入‘0’返回。\n");
	printf("输入‘1’确认删除该型号车辆。\n");
	printf("输入‘0’，返回主菜单。\n");
	scanf("%d", &a);

	if (a == 1) {
		for (i = 0; i <= o; i++) {
			if (0 == strcmp(xh, car[i].xh)) {
				for (k = i; k <= o; k++) {
					strcpy (car[k].id, car[k + 1].id);
					strcpy (car[k].xh, car[k + 1].xh);
					strcpy (car[k].ys, car[k + 1].ys);
					strcpy (car[k].zws, car[k + 1].zws);
					strcpy (car[k].cph, car[k + 1].cph);
					strcpy (car[k].zt, car[k + 1].zt);
				}
			}
		}
		o -= 1;
		printf("已将该型号的车辆全部删除。\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();


	} else {
		showmeun();
	}
}

void look2() {                        //浏览所有乘客
	int i;
	for (i = 0; i <= p - 1; i++) {
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		printf("ID    ：%s\n", peo[i].id);
		printf("姓名  ：%s\n", peo[i].xm);
		printf("工号  ：%s\n", peo[i].gh);
		printf("院系  ：%s\n", peo[i].yx);
		printf("时间  ：%s\n", peo[i].czsj);
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
	}
	printf("任意键返回主菜单。\n");
	system ("pause");
	showmeun();
}

void delpeo() {
	char n;
	system("cls");
	printf("\t\t┌---------------------------------┐\n");
	printf("\t\t│        1.按乘坐时间删除         │\n");
	printf("\t\t│        2.按院系删除             │\n");
	printf("\t\t│        3.按乘客ID删除           │\n");
	printf("\t\t│        4.返回主菜单             │\n");
	printf("\t\t└---------------------------------┘\n");
	printf("\t\t请您选择(1-3):");
	while (1) {
		switch (n) {
			case '1':
				system("cls");
				delpeosj();
				break;
			case '2':
				system("cls");
				delpeoyx();
				break;
			case '3':
				system("cls");
				delpeoid();
				break;
			case '4':
				showmeun();
				break;
			default:
				break;
		}
		scanf("%c", &n);
	}
}

void seek1() {                            //乘客姓名查找
	int i, j = 0;
	char k[10];
	printf("请输入需要查找的乘客姓名：");
	scanf("%s", &k);
	printf("\n");
	for (i = 0; i <= p - 1; i++) {
		if (0 == strcmp(peo[i].id, k)) {
			j++;
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("ID    ：%s\n", peo[i].id);
			printf("姓名  ：%s\n", peo[i].xm);
			printf("工号  ：%s\n", peo[i].gh);
			printf("院系  ：%s\n", peo[i].yx);
			printf("时间  ：%s\n", peo[i].czsj);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
		}
	}

	if (j == 0) {
		printf("此乘客不存在！！！\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();
	}
	printf("任意键返回主菜单。\n");
	system ("pause");
	showmeun();
}

void seek2() {                    //乘客乘坐时间查找
	int i, j = 0;
	char k[10];
	printf("请输入需要查找的乘坐时间（如：周五晚班）：");
	scanf("%s", &k);
	printf("\n");
	for (i = 0; i <= p - 1; i++) {
		if (0 == strcmp(peo[i].czsj, k)) {
			j++;
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("ID    ：%s\n", peo[i].id);
			printf("姓名  ：%s\n", peo[i].xm);
			printf("工号  ：%s\n", peo[i].gh);
			printf("院系  ：%s\n", peo[i].yx);
			printf("时间  ：%s\n", peo[i].czsj);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
		}
	}

	if (j == 0) {
		printf("此乘坐时间的乘客不存在！！！\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();
	}
	printf("任意键返回主菜单。\n");
	system ("pause");
	showmeun();
}

void seek3() {                             //乘客ID查找
	int i, j = 0;
	char k[4];
	printf("请输入查找乘客ID(两位数如:01)：");
	scanf("%s", &k);
	printf("\n");
	for (i = 0; i < p - 1; i++) {
		if (0 == strcmp(k, peo[i].id)) {
			j++;
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("ID    ：%s\n", peo[i].id);
			printf("姓名  ：%s\n", peo[i].xm);
			printf("工号  ：%s\n", peo[i].gh);
			printf("院系  ：%s\n", peo[i].yx);
			printf("时间  ：%s\n", peo[i].czsj);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
		}
	}

	if (j == 0) {
		printf("此ID乘客不存在！！！\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();
	}
	printf("任意键返回主菜单。\n");
	system ("pause");
	showmeun();
}

void delpeosj() {                                         //按乘客乘坐时间删除乘客
	int a, i, k;
	char czsj[10];
	printf("请输入要删除的乘客的乘坐时间：");
	scanf("%s", &czsj );
	for (i = 0; i < p; i++) {
		if (0 == strcmp(czsj, peo[i].czsj)) {
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("ID    ：%s\n", peo[i].id);
			printf("姓名  ：%s\n", peo[i].xm);
			printf("工号  ：%s\n", peo[i].gh);
			printf("院系  ：%s\n", peo[i].yx);
			printf("时间  ：%s\n", peo[i].czsj);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
		}
	}
	printf("已搜索完毕，显示如上，如果结果为空，请输入‘0’返回。\n");
	printf("输入‘1’确认删除该乘客。\n");
	printf("输入‘0’，返回主菜单。\n");
	scanf("%d", &a);

	if (a == 1) {
		for (i = 0; i <= p; i++) {
			if (0 == strcmp(czsj, peo[i].czsj)) {
				for (k = i; k <= p; k++) {
					strcpy (peo[k].id, peo[k + 1].id);
					strcpy (peo[k].xm, peo[k + 1].xm);
					strcpy (peo[k].gh, peo[k + 1].gh);
					strcpy (peo[k].yx, peo[k + 1].yx);
					strcpy (peo[k].czsj, peo[k + 1].czsj);
				}
			}
		}
		p -= 1;
		printf("已将该乘客删除。\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();


	} else {
		showmeun();
	}
}

void delpeoyx() {                                            //按乘客院系删除乘客
	int a, i, k;
	char yx[10];
	printf("请输入要删除的乘客的院系：");
	scanf("%s", &yx );
	for (i = 0; i < p; i++) {
		if (0 == strcmp(yx, peo[i].yx)) {
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("ID    ：%s\n", peo[i].id);
			printf("姓名  ：%s\n", peo[i].xm);
			printf("工号  ：%s\n", peo[i].gh);
			printf("院系  ：%s\n", peo[i].yx);
			printf("时间  ：%s\n", peo[i].czsj);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
		}
	}
	printf("已搜索完毕，显示如上，如果结果为空，请输入‘0’返回。\n");
	printf("输入‘1’确认删除该乘客。\n");
	printf("输入‘0’，返回主菜单。\n");
	scanf("%d", &a);

	if (a == 1) {
		for (i = 0; i <= p; i++) {
			if (0 == strcmp(yx, peo[i].yx)) {
				for (k = i; k <= p; k++) {
					strcpy (peo[k].id, peo[k + 1].id);
					strcpy (peo[k].xm, peo[k + 1].xm);
					strcpy (peo[k].gh, peo[k + 1].gh);
					strcpy (peo[k].yx, peo[k + 1].yx);
					strcpy (peo[k].czsj, peo[k + 1].czsj);
				}
			}
		}
		p -= 1;
		printf("已将该乘客删除。\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();


	} else {
		showmeun();
	}
}

void delpeoid() {                                       //按乘客ID删除
	int a, i, k;
	char id[10];
	printf("请输入要删除的乘客的ID：");
	scanf("%s", &id );
	for (i = 0; i < p; i++) {
		if (0 == strcmp(id, peo[i].id)) {
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("ID    ：%s\n", peo[i].id);
			printf("姓名  ：%s\n", peo[i].xm);
			printf("工号  ：%s\n", peo[i].gh);
			printf("院系  ：%s\n", peo[i].yx);
			printf("时间  ：%s\n", peo[i].czsj);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
		}
	}
	printf("已搜索完毕，显示如上，如果结果为空，请输入‘0’返回。\n");
	printf("输入‘1’确认删除该乘客。\n");
	printf("输入‘0’，返回主菜单。\n");
	scanf("%d", &a);

	if (a == 1) {
		for (i = 0; i <= p; i++) {
			if (0 == strcmp(id, peo[i].id)) {
				for (k = i; k <= p; k++) {
					strcpy (peo[k].id, peo[k + 1].id);
					strcpy (peo[k].xm, peo[k + 1].xm);
					strcpy (peo[k].gh, peo[k + 1].gh);
					strcpy (peo[k].yx, peo[k + 1].yx);
					strcpy (peo[k].czsj, peo[k + 1].czsj);
				}
			}
		}
		p -= 1;
		printf("已将该乘客删除。\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();


	} else {
		showmeun();
	}
}

void delcarid() {                                     //按车辆ID删除车辆
	int a, i, k;
	char id[10];
	printf("请输入要删除的ID：");
	scanf("%s", &id);
	for (i = 0; i < o; i++) {
		if (0 == strcmp(id, car[i].id)) {
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("车辆ID：%s\n", car[i].id);
			printf("型号  ：%s\n", car[i].xh);
			printf("颜色  ：%s\n", car[i].ys);
			printf("牌号  ：%s\n", car[i].zws);
			printf("牌号  ：%s\n", car[i].cph);
			printf("状态  ：%s\n", car[i].zt);
			printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		}
	}
	printf("已搜索完毕，显示如上，如果结果为空，请输入‘0’返回。\n");
	printf("输入‘1’确认删除该ID车辆。\n");
	printf("输入‘0’，返回主菜单。\n");
	scanf("%d", &a);

	if (a == 1) {
		for (i = 0; i <= o; i++) {
			if (0 == strcmp(id, car[i].id)) {
				for (k = i; k <= o; k++) {
					strcpy (car[k].id, car[k + 1].id);
					strcpy (car[k].xh, car[k + 1].xh);
					strcpy (car[k].ys, car[k + 1].ys);
					strcpy (car[k].zws, car[k + 1].zws);
					strcpy (car[k].cph, car[k + 1].cph);
					strcpy (car[k].zt, car[k + 1].zt);
				}
			}
		}
		o -= 1;
		printf("已将该ID车辆全部删除。\n");
		printf("任意键返回主菜单。\n");
		system ("pause");
		showmeun();

	} else {
		showmeun();
	}
}

void inpeople() {
	int i;
	FILE *fp = NULL;
	if ((fp = fopen("inpeoples.txt", "r")) ==
	        NULL) {                              //inpeoples.txt文件需要存在（inpeoples.txt为本地车辆信息文件）
		printf("打开文件失败！");
	}
	fscanf(fp, "%s", peo[0].id);
	fscanf(fp, "%s", peo[0].xm);
	fscanf(fp, "%s", peo[0].gh);
	fscanf(fp, "%s", peo[0].yx);               //先读取头文件，头文件易于文本表格阅读
	fscanf(fp, "%s", peo[0].czsj);
	for (i = 0; i < 6; i++) {              //覆盖头文件
		fscanf(fp, "%s", peo[i].id);
		fscanf(fp, "%s", peo[i].xm);
		fscanf(fp, "%s", peo[i].gh);
		fscanf(fp, "%s", peo[i].yx);
		fscanf(fp, "%s", peo[i].czsj);
	}
}
