#include <stdio.h>
#include <stdlib.h>
#define mazex 6  //迷宫行数
#define mazey 6//迷宫列数
#define starx 1//入口行坐标
#define stary 1//入口列坐标
#define endx 4//出口行坐标
#define endy 4//出口列坐标
int count = 0;	//记录迷宫所有解的次数
int maze[mazex][mazey] = {//迷宫
	{1, 1, 1, 1, 1, 1},
	{1, 0, 0, 0, 0, 1},
	{1, 1, 1, 0, 0, 1},
	{1, 0, 0, 0, 1, 1},
	{1, 1, 0, 0, 0, 1},
	{1, 1, 1, 1, 1, 1}
};
typedef struct {//x为x坐标，y为y坐标，di为下次前进的方向
	int ord, x, y;
	short di;
} ElemType;
typedef struct Node {//链栈结点定义
	ElemType data;
	struct Node*next;
} Node, *LinkList;
typedef struct {//指向链栈的top
	LinkList top;
	int len;
} Lstack;
void initStack(Lstack *s) {//初始化链栈
	s->top = NULL;
	s->len = 0;
}
int push(Lstack *s, ElemType e) {//入栈
	LinkList p = (LinkList)malloc(sizeof(*p));
	if(!p) {
		return 0;
	}
	p->data = e;
	p->next = s->top;
	s->top = p;
	++s->len;
	return 1;
}
int pop(Lstack*s, ElemType *x) {      //出栈
	LinkList q;
	if(!s->top) {
		return 0;
	}
	*x = s->top->data;
	q = s->top;
	s->top = s->top->next;
	--s->len;
	free(q);
	return 1;
}
void destroyStack(Lstack *s) { //销毁链栈
	LinkList q;
	while(s->top) {
		q = s->top;
		s->top = s->top->next;
		free(q);
	}
	s->len = 0;
}
void nextPos(int *x, int*y, short di) {          //判断下一次前进的方向
	switch(di) {
		case 1:
			++(*x);//向左
			break;
		case 2:
			++(*y);//向下
			break;
		case 3:
			--(*x);//向左
			break;
		case 4:
			--(*y);//向右
			break;
		default:
			break;
	}
}
typedef struct {
	int i;
	int j;
	int di;
} Box;
typedef struct {
	Box data[10000];
	int length;
} PathType;
void mazepath(int xi, int yi, int xe, int ye, PathType path) {
	int i = 0, j = 0, y = 0;
	if(xi == xe && yi == ye) { //判断是否已到达终点
		path.data[path.length].i = xi;//将终点坐标进入路径中
		path.data[path.length].j = yi;
		path.length++;
		printf("迷宫路径 %d 如下:", ++count);
		for(int k = 0; k < path.length; k++) {
			printf("(%d,%d,%d),", path.data[k].i, path.data[k].j, path.data[k].di);//打印三元路径
			maze[path.data[k].i][path.data[k].j] = 2;
			y++;
		}
		printf("\n");
		printf("本路径共走%d步\n", y);
		for(int x = 0; x < mazex; x++) {
			for(int z = 0; z < mazey; z++) { //以方阵形式输出路径
				printf("%3d", maze[x][z]);
			}
			printf("\n");
		}
		printf("\n");
	} else {
		if(maze[xi][yi] == 0) {
			int di = 0;	//用于四个方位移动的变量
			while(di < 4) {
				path.data[path.length].i = xi;
				path.data[path.length].j = yi;
				path.data[path.length].di = di;//第一步：将起点(xi,yi，di)进入path路径中
				path.length++;
				//对四个方位寻找相邻方块，判断是否有路
				switch(di) {
					case 0: {
						i = xi - 1, j = yi;
						break;
					}
					case 1: {
						i = xi, j = yi + 1;
						break;
					}
					case 2: {
						i = xi + 1, j = yi;
						break;
					}
					case 3: {
						i = xi, j = yi - 1;
						break;
					}
				}
				//第二步：将maze[xi][yi]=-1，避免来回走动
				maze[xi][yi] = -1;
				//第三步：递归调用迷宫函数求解小问题
				mazepath(i, j, xe, ye, path);
				//第四步：回退path数组中的元素，并将回退元素maze[xi][yi]=0来寻找其他路径
				path.length--;
				maze[xi][yi] = 0;
				di++;
			}
		}
	}
}
int main() {
	printf("原迷宫：\n");
	PathType path;
	path.length = 0;
	for(int x = 0; x < mazex; x++) {
		for(int z = 0; z < mazey; z++) {
			printf("%3d", maze[x][z]);
		}
		printf("\n");
	}
	printf("递归求所有路径：\n");
	mazepath(starx, stary, endx, endy, path);
	maze[endx][endy] = 0;
	Lstack s, t;
	int x, y, curstep, i = 0;
	ElemType e;
	initStack(&s);
	initStack(&t);
	printf("******************************************************\n非递归求路径\n迷宫为：\n");
	for(x = 0; x < mazex; x++) {
		for(y = 0; y < mazey; y++) {
			printf("%3d", maze[x][y]);
		}
		printf("\n");
	}
	x = starx, y = stary;
	curstep = 1;
	do {
		if(maze[y][x] == 0) {
			maze[y][x] = 2;//标记已经走过的路径
			e.x = x;
			e.y = y;
			e.di = 1;
			e.ord = curstep;
			if(!push(& s, e)) {
				printf("入栈失败！");
			}
			if(x == endx && y == endy) {
				break;
			}
			nextPos(&x, &y, 1);
			curstep++;
		} else {
			if(s.len != 0) {
				pop(&s, &e);
				while(e.di == 4 && s.len != 0) {
					maze[e.y][e.x] = -1;//di已经等于4了，又不可能往回走，说明此次不属于解的路径，赋值为-1，防止下次再次进入
					pop(&s, &e);//把不是解的路径出栈
				}
				if(e.di < 4) {
					e.di++;
					if(!push(&s, e)) {
						printf("入栈失败！");
					}
					x = e.x, y = e.y;
					nextPos(&x, &y, e.di);
				}
			}
		}
	} while(s.len != 0);
	printf("试探过程路线：\n");
	printf("1表示墙，0表示通路，-1表示已经探索但不可行的路，2表示通往出口的路径\n");
	for(x = 0; x < mazex; x++) {
		for(y = 0; y < mazey; y++) {
			printf("%3d", maze[x][y]);
			if(maze[x][y] == 2) {
				i++;
			}
		}
		printf("\n");
	}
	printf("探索步数：%d\n", i);
	for(x = 0; x < i; x++) {
		pop(&s, &e);
		push(&t, e);
	}
	printf("路径：");
	while(t.len != 0) {
		printf("(%d,%d,%d),", t.top->data.x, t.top->data.y, t.top->data.di);
		pop(&t, &e);
	}
	destroyStack(&s);
	getchar();
	return 0;
}
